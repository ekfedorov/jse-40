package ru.ekfedorov.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.other.ISaltSetting;

public interface HashUtil {

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @Nullable final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull final java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    static String salt(
            @Nullable final ISaltSetting setting,
            @Nullable final String value
    ) {
        if (setting == null) return null;
        @Nullable final String secret = setting.getPasswordSecret();
        @Nullable final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @Nullable
    @SneakyThrows
    static String salt(
            @Nullable final ISaltSetting setting,
            @Nullable final Object value
    ) {
        if (setting == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(value);
        @Nullable final String secret = setting.getSignSecret();
        @Nullable final Integer iteration = setting.getSignIteration();
        return salt(secret, iteration, json);
    }

    @Nullable
    static String salt(
            @Nullable final String secret,
            @Nullable final Integer iteration,
            @Nullable final String value
    ) {
        if (value == null) return null;
        if (iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

}
