package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.Task;
import ru.ekfedorov.tm.endpoint.TaskEndpoint;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show task by name.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-view-by-name";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @NotNull final Task task = taskEndpoint.findTaskOneByName(session, name);
        if (task == null) throw new NullTaskException();
        showTask(task);
        System.out.println();
    }

}
