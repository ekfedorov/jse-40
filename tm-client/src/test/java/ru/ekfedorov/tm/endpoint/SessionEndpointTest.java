package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class SessionEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void openSession() {
        final Session session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        Assert.assertNotNull(session);
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test(expected = AccessDeniedException_Exception.class)
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void closeSession() {
        final Session session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.getTaskEndpoint().findAllTask(session);
    }

}
