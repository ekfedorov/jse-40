package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.model.AbstractEntity;


public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
