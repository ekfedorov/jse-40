package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.IUserEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.exception.empty.EmailIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.empty.PasswordIsEmptyException;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserOneBySession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws NullSessionException, AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getUserService().findOneById(session.getUserId()).orElse(null);
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws LoginIsEmptyException, PasswordIsEmptyException, EmailIsEmptyException {
        serviceLocator.getUserService().create(login, password, email);
    }

    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login).orElse(null);
    }

    @Override
    @WebMethod
    public void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        serviceLocator.getUserService().updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
