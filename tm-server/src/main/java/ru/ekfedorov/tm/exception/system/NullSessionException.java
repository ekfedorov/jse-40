package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public final class NullSessionException extends AbstractException {

    public NullSessionException() {
        super("Error! Session is null...");
    }

}
