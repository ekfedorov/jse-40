package ru.ekfedorov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.model.Session;

public interface ISessionService extends IService<Session> {

    @Nullable Session close(@Nullable Session session);

    @Nullable
    Session open(String login, String password);

    void validate(@Nullable Session session);

    void validateAdmin(@Nullable Session session, @Nullable Role role);

    @SneakyThrows
    void remove(@Nullable Session entity);
}
