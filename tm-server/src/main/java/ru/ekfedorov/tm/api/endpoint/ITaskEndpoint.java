package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    Task addTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException, NullSessionException;

    void changeTaskStatusById(
            @NotNull Session session,
            @NotNull String id,
            @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    void changeTaskStatusByIndex(
            @NotNull Session session,
            @NotNull Integer index,
            @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    void changeTaskStatusByName(
            @NotNull Session session,
            @NotNull String name,
            @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws AccessDeniedException, NullSessionException ;

    @WebMethod
    void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<Task> findTaskAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull String sort
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    void finishTaskById(
            @NotNull Session session,
            @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    void finishTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    void finishTaskByName(
            @NotNull Session session,
            @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    void startTaskById(
            @NotNull Session session,
            @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    void startTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    void startTaskByName(
            @NotNull Session session,
            @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException, NullSessionException;

    void updateTaskById(
            @NotNull Session session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

    void updateTaskByIndex(
            @NotNull Session session,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

}
